**Bariatric surgery (weight loss surgery)**



**Why surgery for weight loss ?**
 
Obesity is a very morbid condition that has several health consequences. Mild obesity is still controllable by nutrition and exercise. However as it advances it becomes more difficult because of metabolic changes, physical limitations, psychological effect and social inhibition.
When exercise and dieting does not help control the weight [bariatric surgery](http://www.bariatricsurgery4health.com/) may help loose weight and bring a person back to ground Zero where he can start working on his lifestyle and maintain weight.
 
So What are the different procedures to reduce weight?
 
There are two ways it can help loose weight-

 
**Restrictive  surgery.**

In these procedures the stomach is made smaller. A section of your stomach is removed that helps to limits the amount of food it can hold and causes you to feel full.


**Malabsorptive  surgery.**

Most of digestion and absorption takes place in the small intestine. Surgery to this area shortens the length of the small intestine and/or changes where it connects to the stomach, limiting the amount of food that is completely digested or absorbed.
Through food intake restriction, malabsorption or a combination of both, you can lose weight since less food either goes into your stomach or stays in your small intestine long enough to be digested and absorbed
Gastric Bypass
As “gastric bypass surgery” implies, this surgical procedure routes food past most of the stomach and the first part of the small intestine. In addition to restricting food intake, Gastric Bypass reduces nutrient absorption.

**Gastric bypass surgery involves three main steps:**

1. Creating a new, smaller stomach (gastric pouch) from the existing stomach.
2. Bypassing 3 to 4 ft. of the small intestine (normally 20 ft. long).
3. Connecting the bypassed digestive section to the lower section of the small intestine, where necessary digestive juices mix with food.

The small pouch of stomach can hold about 1/3 cup of food. After this, it begins to stretch, giving you the feeling of fullness. Although in most people it will give a feeling of fullness, in some people the stretching may give a feeling of discomfort. The small stomach pouch will need time to digest the food and push it through the GJ opening into the small bowel. If you do not give enough time to do this, you may start vomiting.

Post-Op Dietary Plan for Gastric Bypass Surgery Patients
As with all surgical weight-loss programs, it is imperative that patients adhere to dietary recommendations following surgery. For the first 18 months after gastric bypass surgery, patients must restrict themselves to consuming less than 800 calories per day; after 36 months, patients may consume no more than 1200 calories each day. In addition, patients must avoid eating sugars and fats to prevent "Dumping Syndrome."
Surgical Procedure and Hospital Stay -Over 98% of Gastric Bypass Surgery RYGB’s are performed laparoscopically, with an average of 6 small incisions. They all require full general anesthesia and about 2 hours of surgical time. Most patients stay one or possibly two nights in the hospital.


**Adjustable Gastric Banding**

The Newest and Safest Surgical Weight-Loss Procedure
Approved by the FDA in 2001, Lap Band® Surgery is the safest and least invasive bariatric surgery for weight-loss patients. The LapBand® is a small, inflatable belt placed around the upper portion of the patient's stomach that functions to restrict the amount of food consumed, slow the emptying of food from the stomach, and provide a constant sensation of being "full." Like our other weight-loss surgeries, Laparoscopic gastric banding surgery is a minimally invasive. Operating time is about 60 minutes and most patients are ready to leave the hospital the following day.
Gastric banding stomach reduction surgery functions by limiting the amount of food a patient can consume and delaying the emptying of the stomach to create the sensation of constantly being "full."
On average, patients who undergo Lap Band surgery experience a 60% loss of excess weight.

**How much weight loss is achieved?**

The vast majority of patients lose 50 to 80% of their excess weight in the first 12 months of Weight Loss after  gastric bypass surgery. With the gastric bypass, most patients keep 50% to 70% of their excess weight off after 10 years. However, some patients can regain some or much of their original weight.
After the first 2 years, there is a very slow weight loss depending on the eating habits and exercise commitment of the patient. If you successfully use the preoperative period to adapt your lifestyle and train yourself to eat differently, this small pouch helps to remind us to eat less food. Thus, the intake of food is dramatically limited and enables us to lose weight, and keep the weight off lifelong. 
Although there is some malabsorption of certain types of minerals and vitamins as described above, the food that is eaten will be well digested and nearly completely absorbed. This enables us to maintain good nutritional status and remain healthy with proper eating.
